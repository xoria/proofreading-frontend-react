import {Component} from "preact";
import PropTypes from "prop-types";
import linkState from "linkstate";

/*==================================================== Component  ====================================================*/

export default class NewCommentForm extends Component {

  static propTypes = {
    authority: PropTypes.any.isRequired,
  };

  state = {
    title: "",
    tags: [],
    body: "",
  };

  render() {
    const {title, tags, body} = this.state;
    // eslint-disable-next-line no-unused-vars
    const {authority, ...props} = this.props;
    return (
        <form {...props} id={"new-comment-form"} onSubmit={this.addComment}>
          <div className="form-group">
            <label className="form-label d-none" htmlFor="new-comment-title">Title</label>
            <div className="input-group">
              <input className="form-input" type="text" id="new-comment-title" placeholder="Title" value={title}
                     onChange={linkState(this, "title")}/>
              <i className={"input-group-addon fas fa-highlighter text-gray tooltip"}
                 data-tooltip={"Select some text and use the popup menu\nto add a comment mark"}/>
            </div>
          </div>
          <div className="form-group">
            <label className="form-label d-none" htmlFor="new-comment-labels">Label1, Label2, ...</label>
            <input className="form-input" type="text" id="new-comment-labels" value={tags.join(", ")}
                   placeholder="Label1, Label2, ..." onChange={this.updateTags}/>
          </div>
          <div className="form-group">
            <label className="form-label d-none" htmlFor="new-comment-body">Labels</label>
            <textarea className="form-input" id="new-comment-body" placeholder="Content" value={body}
                      onChange={linkState(this, "body")}></textarea>
          </div>
          <div className={"mb-2 d-flex"}>
            <button type={"reset"} className={"btn btn-error"}>
              Reset
            </button>
            <button type={"submit"} className={"btn btn-primary "}>
              Submit
            </button>
          </div>
        </form>
    );
  }

  updateTags = (evt) => this.setState({tags: evt.target.value.split(",").map(t => t.trim())});

  addComment = (evt) => {
    evt.preventDefault();
    const authority = this.props.authority;
    const {title, tags, body} = this.state;
    const id = authority.nextId();
    authority.emit("comment:new", {id, title, tags, body}); // todo handle
  };

}
