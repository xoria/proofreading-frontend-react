import "./style.scss";

import {Link} from "preact-router/match";

/*===================================================== Exports  =====================================================*/

export default Navigation;

/*==================================================== Component  ====================================================*/

function Navigation() {
  return (
      <div className="navbar">
        <div className="container">
          <section className="navbar-section">
            <Link activeClassName="active" className="btn btn-lg btn-link" href="/">Home</Link>
            <Link activeClassName="active" className="btn btn-lg btn-link" href="/documents">Documents</Link>
          </section>
        </div>
      </div>
  );
}
