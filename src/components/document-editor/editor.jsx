import {EditorView} from "prosemirror-view";
import {EditorState} from "prosemirror-state";
import {history as historyPlugin} from "prosemirror-history";
import {keymap as keymapPlugin} from "prosemirror-keymap";
import RebasePlugin, {
  dropTransaction,
  getState as getRebasePluginState,
  rebaseTransaction
} from "../../services/prosemirror-rebase";
import {Component, createRef} from "preact";
import keymap from "../../constants/editor-keymap";
import PropTypes from "prop-types";
import {rebaseRemoteState} from "../../services/document-authority";

const REBASE_TRANSACTION_OPTIONS = {clearHistory: true, exportBiSteps: true};

/*===================================================== Exports  =====================================================*/

export default class Editor extends Component {

  static propTypes = {
    authority: PropTypes.any.isRequired,
  };

  pmRef = createRef();

  async componentDidMount() {
    const authority = this.props.authority;
    const editorState = createEditorState(await authority.getState());
    this.view = new EditorView(null, {state: editorState, dispatchTransaction: this._dispatchTransaction});
    this.pmRef.current.appendChild(this.view.dom);
    this.__removeFromAuthority = authority.on("document:foreign:update", this.receive);
  }

  async componentWillUnmount() {
    await this.props.authority.ready;
    this.__removeFromAuthority();
    this.view.destroy();
  }

  render(props) { return <div {...props} ref={this.pmRef}/>; }

  receive = ({baseVersion, steps}) => {
    const transaction = rebaseTransaction(this.view.state, baseVersion, steps, REBASE_TRANSACTION_OPTIONS);
    this.view.dispatch(transaction);
  };

  _dispatchTransaction = async (transaction) => {
    // update state with applied transaction
    // eslint-disable-next-line prefer-reflect
    this.view.updateState(this.view.state.apply(transaction));
    // notify authority of updated state
    const authority = this.props.authority;
    const view = this.view;
    const {baseVersion, biSteps, maxVersion: version} = getRebasePluginState(view.state);
    const inverseSteps = biSteps.map((step) => step.backward);
    authority.emit("state:update", {version, inverseSteps, doc: view.state.doc});
    // if any steps queued, process by authority
    if (biSteps.length > 0) {
      const success = (await authority.reduce("document:local:publish", {baseVersion, biSteps})) !== false;
      if (success) { view.dispatch(dropTransaction(view.state, version)); }
    }
  };

}

function createEditorState(authorityState) {
  const {baseVersion, biSteps} = rebaseRemoteState(authorityState, true);
  return EditorState.create({
    doc: authorityState.doc,
    plugins: [
      historyPlugin(),
      keymapPlugin(keymap),
      new RebasePlugin(baseVersion, biSteps),
    ],
  });
}
