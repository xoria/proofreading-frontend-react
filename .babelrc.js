module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        targets: [">3%", "not dead"],
      }
    ]
  ],
  plugins: [
    ["@babel/plugin-transform-react-jsx", {pragma: "Preact.h", useBuiltIns: true}],
    ["@babel/plugin-proposal-decorators", {legacy: true}],
    ["@babel/plugin-proposal-class-properties", {loose: true}],
  ]
};
